//
//  ViewController.swift
//  style_shaker
//
//  Created by Etudiant on 07/06/2016.
//  Copyright © 2016 Etudiant. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

   
    @IBAction func switch_hair(sender: AnyObject) {
        if (hair == true){
            hair = false
        }
        else if (hair == false){
            hair = true
        }
        print(hair)
    }
    @IBAction func switch_skinn(sender: AnyObject) {
        if (skinn == true){
            skinn = false
        }
        else if (skinn == false){
            skinn = true
        }
        print(skinn)
    }
    @IBAction func switch_sex(sender: AnyObject) {
        if (sex == true){
            sex = false
        }
        else if (sex == false){
            sex = true
        }
        print(sex)
    }
    var hair:Bool = true
    var skinn:Bool = true
    var sex:Bool = true
    
    @IBAction func next_to_mood(sender: AnyObject) {
        self.performSegueWithIdentifier("next_to_mood", sender: nil)
    }
    @IBAction func next_to_research(sender: AnyObject) {
        self.performSegueWithIdentifier("next_to_research", sender: nil)
    }
    
    @IBOutlet weak var work_switch: UISwitch!
    @IBOutlet weak var party_switch: UISwitch!
    @IBOutlet weak var weekend_switch: UISwitch!
    @IBOutlet weak var chill_switch: UISwitch!
    
}

